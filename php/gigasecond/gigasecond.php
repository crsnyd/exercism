<?php
    function from($date) {
      if(is_object($date)){
            $gs = clone $date;
            $gs->add(new DateInterval('PT1000000000S'));
            return $gs; 
        }else{
            throw new InvalidArgumentException('Expecting Date Object');
        }
    }
?>