<?php

//
// This is only a SKELETON file for the "Hamming" exercise. It's been provided as a
// convenience to get you started writing code faster.
//
function  getLetterArray($word){
	return str_split($word);
}
function distance($a, $b)
{      $arrLetter = getLetterArray($a);
       $arrLetter2 =  getLetterArray($b);	
       if(count($arrLetter) === count($arrLetter2)){
           $result = array_diff_assoc($arrLetter, $arrLetter2);
           return count($result);
       }else{
           throw new InvalidArgumentException('DNA strands must be of equal length.');
       }
       
}


