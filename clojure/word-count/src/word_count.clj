(ns word-count
  (:require [clojure.string :as s]))

(defn remove-all-but-useful [word]
 (->> (s/replace word #"[^a-zA-Z0-9\s]" "")))

(defn split-on-whitespace [sentence] 
  (->> (s/split sentence #"\s")
   (filter #(not (s/blank? %))))) 

(defn word-count [sentence]
  (as-> (remove-all-but-useful sentence) $
    (s/lower-case $)
    (split-on-whitespace $)
    (frequencies $)))
