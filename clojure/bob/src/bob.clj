(ns bob
  (:require [clojure.string :as s]))

(defn is-yelling [words]
  (and (not (= (s/lower-case words) words)) 
           (= (s/upper-case words) words)))

(defn is-question [words]
  (s/ends-with? words "?"))

(defn response-for [words]
  (if (is-yelling words) "Whoa, chill out!"
    (if (is-question words) "Sure." 
      (if (s/blank? words) "Fine. Be that way!" "Whatever."))))

