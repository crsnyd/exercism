(ns anagram
  (:require [clojure.string :as s]))

(defn same-letters? [word other-word]
  (= (sort (s/lower-case word))
     (sort (s/lower-case other-word))))

(defn anagram?
  [word potential-anagram]
  (and (not= word potential-anagram)
       (same-letters? word potential-anagram)))

(defn anagrams-for
  [word potential-anagrams]
  (filter (partial anagram? word) potential-anagrams))

