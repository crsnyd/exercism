(ns hello-world)

(defn hello([] (hello "World"))          
   ([name](let [base-word "Hello, "
                second-word (str name "!")]
           (str base-word second-word))))
