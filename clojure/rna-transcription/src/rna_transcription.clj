(ns rna-transcription
  (:require [clojure.string :as str]))

(defn to-rna [dna]
  (->> (map #(let [nucleotide (str %)]
               (cond 
                 (= "G" nucleotide) "C"
                 (= "C" nucleotide) "G"
                 (= "A" nucleotide) "U"
                 (= "T" nucleotide) "A"
                 :else (throw (AssertionError. (str "DNA typo on " nucleotide)))))
            dna)
       (str/join))) 
