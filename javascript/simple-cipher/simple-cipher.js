const startMarker = 'a'.charCodeAt(0);
const endMaker = 'z'.charCodeAt(0)

const isLetter = (letter =>{ letter.toLowerCase() != letter.toUpperCase()});
const isEncoded = (letter =>{between(letter startMarker, endMaker)})
const returnNumber = (num =>{
	if( num > endmaker){
		return (startMaker +(num - endmaker));
	}else{
		return num;
	}
})

export const encode = (letter => {
	if(isLetter(letter)){
		let letterNum = returnNumber(3 + letter.charCodeAt(0)); 
		return String.fromCharCode(letterNum);
	}else{
		throw "Invaild Input";
	}
});

export const decode = (letter => {
	if(isEncoded(letter)){
		let letterNum = returnNumber(3 - letter.charCodeAt(0));
		return String.fromCharCode(letterNum);
	}else{
		throw "Invaild Input";
	}
});

