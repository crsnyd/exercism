var Cipher = require('./simple-cipher');

describe('Random key cipher', function () {

  // Here we take advantage of the fact that plaintext of "aaa..."
  // outputs the key. This is a critical problem with shift ciphers, some
  // characters will always output the key verbatim.
  xit('can encode', function () {
    expect(encode('aaaaaaaaaa')).toEqual(cipher.key.substr(0, 10));
  });

  xit('can decode', function () {
    expect(decode(cipher.key.substr(0, 10))).toEqual('aaaaaaaaaa');
  });

  xit('is reversible', function () {
    var plaintext = 'abcdefghij';
    expect(decode(encode(plaintext))).toEqual(plaintext);
  });
});

