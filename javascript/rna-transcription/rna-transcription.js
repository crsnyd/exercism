var DnaTranscriber = function (){};

var translations = {
	A: 'U',
	T: 'A',
	C: 'G',
	G: 'C'
};

DnaTranscriber.prototype.toRna = function(strand) {
	strand = strand.split('');
	return strand.map(getTranslate).join('');
}

function getTranslate (nucleotide){
	var translated = translations[nucleotide];
	if(translated == ''){
		throw "Invaild Input";
	}else{
		return translated
	}
}

module.exports = DnaTranscriber;
