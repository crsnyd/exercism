module Bob exposing (..)

import String exposing (..)


hey : String -> String
hey word =
    if (toUpper word) == word && (toLower word) /= word then
        "Whoa, chill out!"
    else if endsWith "?" word then
        "Sure."
    else if isEmpty (trim word) then
        "Fine. Be that way!"
    else
        "Whatever."
